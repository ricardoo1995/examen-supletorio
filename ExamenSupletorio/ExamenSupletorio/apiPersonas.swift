//
//  apiPersonas.swift
//  ExamenSupletorio
//
//  Created by Ricardo Ortiz on 21/8/18.
//  Copyright © 2018 Ricardo Ortiz. All rights reserved.
//
import Foundation

protocol SessionProtocol {
    func dataTask(
        with url: URL,
        completionHandler: @escaping
        (Data?, URLResponse?, Error?) -> Void)
        -> URLSessionDataTask
}

enum WebserviceError : Error {
    case DataEmptyError
    case ResponseError
}

extension URLSession: SessionProtocol {}

class apiPersona: SessionProtocol  {
    
    lazy var session: SessionProtocol = URLSession.shared
    
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return URLSession.shared.dataTask(with: url)
    }
    func dataPersona(withName nombre: String,
                     apellido: String, email: String,
                   completion: @escaping (Token?, Error?) -> Void) {
        
        let allowedCharacters = CharacterSet(
            charactersIn: "/%&=?$#+-~@<>|\\*,.()[]{}^!").inverted
        guard let encodedNombre = nombre.addingPercentEncoding(
            withAllowedCharacters: allowedCharacters) else { fatalError() }
        guard let encodedApellido = apellido.addingPercentEncoding(
            withAllowedCharacters: allowedCharacters) else { fatalError() }
        guard let encodedEmail = email.addingPercentEncoding(
            withAllowedCharacters: allowedCharacters) else { fatalError() }
        
        let query = "nombre=\(encodedNombre)&apellido=\(encodedApellido)&email=\(encodedEmail)"
        guard let url = URL(string: "https://private-534ec-amstronghuang.apiary-mock.com/members\(query)") else {
            fatalError()
        }
        
        session.dataTask(with: url) { (data, response, error) in
            guard error == nil else {
                completion(nil, WebserviceError.ResponseError)
                return
            }
            guard let data = data else {
                completion(nil, WebserviceError.DataEmptyError)
                return
            }
            do {
                let dict = try JSONSerialization.jsonObject(
                    with: data,
                    options: []) as? [String:String]
                let token: Token?
                if let tokenString = dict?["token"] {
                    token = Token(id: tokenString)
                } else {
                    token = nil }
                completion(token, nil)
            } catch {
                completion(nil, error)
            }
            }.resume()
    }
}
