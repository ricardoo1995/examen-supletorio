//
//  personasManager.swift
//  ExamenSupletorio
//
//  Created by Ricardo Ortiz on 21/8/18.
//  Copyright © 2018 Ricardo Ortiz. All rights reserved.
//

import Foundation

internal class ItemManager : NSObject {
    
    internal var toDoCount: Int { get }
    
    internal var doneCount: Int { get }
    
    internal var toDoPathURL: URL { get }
    
    internal var donePathURL: URL { get }
    
    override internal init()
    
    internal func add(_ item: ToDoItem)
    
    internal func item(at index: Int) -> ToDoItem
    
    internal func checkItem(at index: Int)
    
    internal func uncheckItem(at index: Int)
    
    internal func doneItem(at index: Int) -> ToDoItem
    
    internal func removeAll()
    
    @objc internal func save()
}

