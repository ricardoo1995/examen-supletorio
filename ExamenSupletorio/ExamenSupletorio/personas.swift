//
//  personas.swift
//  ExamenSupletorio
//
//  Created by Ricardo Ortiz on 21/8/18.
//  Copyright © 2018 Ricardo Ortiz. All rights reserved.
//
import Foundation

struct personas : Equatable {
    
    let nombre: String
    let apellido: String?
    let email: String?
    
    private let nombreKey = "nombreKey"
    private let apellidoKey = "apellidoKey"
    private let emailKey = "emailKey"
   
    var plistDict: NSDictionary {
        var dict = [String:AnyObject]()
        dict[nombreKey] = nombre as AnyObject
        if let apellido = apellido {
            dict[apellidoKey] = apellido as AnyObject
        }
        if let email = email {
            dict[emailKey] = email as AnyObject
        }
    
        return dict as NSDictionary
    }
    
    init(
        nombre: String,
        apellido: String,
        email: String
        
        ) {
        self.nombre = nombre
        self.apellido = apellido
        self.email = email
        
    }
    
    init?(dict: NSDictionary){
        guard let nombre = dict[nombreKey] as? String else
        { return nil }
        self.nombre = nombre
        self.apellido = dict[apellidoKey] as? String
        self.email = dict[emailKey] as? String
    }
        
    
    static func ==(lhs: personas, rhs: personas) -> Bool {
        
        if lhs.email != rhs.email {
            return false
        }
        if lhs.apellido != rhs.apellido {
            return false
        }
        if lhs.nombre != rhs.nombre {
            return false
        }
        return true
    }
}


